export interface post{
    id?:number,
    created_at?: string,
    updated_at?: string,
}

export interface HomeworkType extends post{
    class_id?: number,
    homework_data?: object,
    name?:string,
    teacher_id?: number,
    Jobs?:Job[]
}

export interface Job extends post{
    homeworks_id?: number,
    students_id?: number,
    state?: number,
    achievement?: number,
    jobsdata?: null | string | object,
    Homework?: null | HomeworkType
}

export interface ClassType extends post{
    Teachers_grade?:object,
    class_id?: number,
    class_name?: string,
}

export interface ChooseType{
    title:string,
    options: {
      A: string,
      B: string,
      C?: string,
      D?:string,
    },
    difficulty: number,
    grouping: string,
    answer: string,
    score?:number,
    studentAnswer?: string
}

export interface LoginType {
    name: string,
    password: string,
    userType: string
}

export interface successType {
    code:number,
    msg:string,
    data:any
}