
import http from "../http";
import {LoginType} from '../interface'
export default new class Login{ 
    userLogin(data: LoginType) {
        return http.request({
            url: `/admin/login`,
            method: "post",
            data
        })
    }

    addUser(data: any) {
        return http.request({
            url: "/admin/addAdmin",
            method: "post",
            data
        })
    }
}