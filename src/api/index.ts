import Login from './Login'
import Home from './Home'
import Homework from './Homework'
import Student from './Student'
import Teacher from './Teacher'
export {
    Login,
    Home,
    Homework,
    Student,
    Teacher
}