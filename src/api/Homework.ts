import Http from '../http'
export default new class Homework{ 

    issueChoose(data: any) {
        return Http.request({
            url: `/admin/issueChoose`,
            method: "post",
            data
        })
    }
    getChooseMax(){
        return Http.request({
            url: `/admin/getChooseMax`,
            method: "get"
        })
    }
    getChoose(data: any) {
        return Http.request({
            url: `/admin/getChoose`,
            method: "post",
            data
        })
    }
    getHomeworkByTeacherIdMax(data: any) {
        return Http.request({
            url: `/admin/getHomeworkByTeacherIdMax`,
            method:"post",
            data
        })
    }
    getHomeworkByTeacherIdPage(data: any) {
        return Http.request({
            url: `/admin/getHomeworkByTeacherIdPage`,
            method: "post",
            data
        })
    }
    homeworkIssue(data: any) {
        return Http.request({
            url: `/admin/homeworkIssue`,
            method: "post",
            data
        })
    }
    getChooseByGrouping(data: any) {
        return Http.request({
            url: `/admin/getChooseByGrouping`,
            method: "post",
            data
        })
    }
    getJobByStudentIdHomeworkId(data: any) {
        return Http.request({
            url: `/admin/getJobByStudentIdHomeworkId`,
            method: "post",
            data
        })
    }
    getHomeworkByJobId(data: any) {
        return Http.request({
            url: `/admin/getHomeworkByJobId`,
            method: "post",
            data
        })
    }
    writingHomework(data:any){
        return Http.request({
            url: `/admin/writingHomework`,
            method: "post",
            data
        })
    }
}