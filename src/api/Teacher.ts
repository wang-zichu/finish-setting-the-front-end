
import http from "../http";

export default new class Teacher{ 
    getClassListByTeacherId(data: any) {
        return http.request({
            url: `/admin/getClassListByTeacherId`,
            method: "post",
            data
        })
    }
}