import Http from '../http'
export default new class Home{ 

    getHomeworkByTeacherId(data: any) {
        return Http.request({
            url: `/admin/getHomeworkByTeacherId`,
            method: "post",
            data
        })
    }

    getClassListByTeacherId(data: any){
        return Http.request({
            url: `/admin/getClassListByTeacherId`,
            method: "post",
            data
        })
    }
}