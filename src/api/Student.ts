
import http from "../http";

export default new class Student{ 
    getJobListByStudentId(data: any) {
        return http.request({
            url: `/admin/getJobListByStudentId`,
            method: "post",
            data
        })
    }
    getStudentMax(data: any) {
        return http.request({
            url: `/admin/getStudentMax`,
            method: "post",
            data
        })
    }
    getHomeworkByStudentIdPage(data: any){
        return http.request({
            url: `/admin/getHomeworkByStudentIdPage`,
            method: "post",
            data
        })
    }
    getUserByStudentId(data: any) {
        return http.request({
            url: `/admin/getUserByStudentId`,
            method: "post",
            data
        })
    }
    getJobByStudentIdAndHomeworkId(data: any) {
        return http.request({
            url: `/admin/getJobByStudentIdAndHomeworkId`,
            method: "post",
            data
        })
    }
}