import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { axiosPlugin } from './plugins/axios'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

createApp(App).use(ElementPlus).use(store).use(axiosPlugin).use(router).mount('#app')
