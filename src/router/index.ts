import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'login',
    component: () => import('../views/LoginView.vue')
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/HomeView.vue'),
    children:[
      {
        path: '/teacherHome',
        name: 'teacherHome',
        component: () => import('../views/teacher/HomeTeacherView.vue'),
      },
      {
        path: '/studentHome',
        name: 'studentHome',
        component: () => import('../views/student/HomeStudentView.vue'),
      },
      {
        path: '/questionBank',
        name: 'questionBank',
        component: () => import('../views/teacher/QuestionBankView.vue'),
      },
      {
        path: '/workPublished',
        name: 'workPublished',
        component: () => import('../views/teacher/WorkPublishedView.vue'),
      },
      {
        path: '/HomeworkListView',
        name: 'HomeworkListView',
        component: () => import('../views/student/HomeworkListView.vue'),
      }, 
      {
        path: '/WritingHomeworkView',
        name: 'WritingHomeworkView',
        component: () => import('../views/student/WritingHomeworkView.vue'),
      },
      {
        path:'/PersonalView',
        name:'PersonalView',
        component: () => import('../views/teacher/PersonalView.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
